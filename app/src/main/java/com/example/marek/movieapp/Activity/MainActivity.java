package com.example.marek.movieapp.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.marek.movieapp.R;


public class MainActivity extends AppCompatActivity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
}

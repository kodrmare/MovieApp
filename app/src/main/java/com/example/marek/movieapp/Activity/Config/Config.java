package com.example.marek.movieapp.Activity.Config;

import android.app.Application;
import android.content.Context;

import com.example.marek.movieapp.BuildConfig;


/**
 * Created by marek on 20/07/2017.
 */

public class Config
{
	public static final boolean LOGS = BuildConfig.LOGS;
	public static final boolean DEV_ENVIRONMENT = BuildConfig.DEV_ENVIRONMENT;

	public static final String REST_BASE_URL_PROD = "http://example.com/api/";
	public static final String REST_BASE_URL_DEV = "http://dev.example.com/api/";
	public static final String REST_BASE_URL = DEV_ENVIRONMENT ? REST_BASE_URL_DEV : REST_BASE_URL_PROD;
}
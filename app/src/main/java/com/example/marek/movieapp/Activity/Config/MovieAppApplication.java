package com.example.marek.movieapp.Activity.Config;

import android.app.Application;
import android.content.Context;


/**
 * Created by marek on 20/07/2017.
 */

public class MovieAppApplication extends Application
{
	private static MovieAppApplication sInstance;


	public MovieAppApplication()
	{
		sInstance = this;
	}


	public static Context getContext()
	{
		return sInstance;
	}


	@Override
	public void onCreate()
	{
		super.onCreate();




	}
}